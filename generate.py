from __future__ import print_function
from argparse import ArgumentParser, RawTextHelpFormatter
import numpy as np
from scipy.stats import norm, rv_continuous
# Uncomment next two lines for running as script without graphics output
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab

tab_entries = 1024

### ARGUMENT PARSER ###
parser = ArgumentParser(formatter_class=lambda prog: RawTextHelpFormatter(prog, width=80))
parser.add_argument("-a", "--min_frag_size", dest="min_frag_size", type=float, default=0.,
                    help="minimum fragment size, default: %(default)s")

parser.add_argument("-b", "--max_frag_size", dest="max_frag_size", type=float, default=400.,
                    help="maximum fragment size, default: %(default)s")

parser.add_argument("-nfs", "--number_fz", dest="prob_intervals", type=int, default=64,
                    help="nominal number of address groups, default: %(default)s")

parser.add_argument("-d", "--distr", dest="dist_choice", default='single',
                    help="single (normal pdf) or double (bi-normal pdf), default: %(default)s")

parser.add_argument("-m", "--mu", dest="mu1",  type=float, default=200.,
                    help="centroid, default: %(default)s")

parser.add_argument("-s", "--sigma", dest="sigma1",  type=float, default=50., 
                    help="standard deviation, default: %(default)s")

parser.add_argument("-m2", "--mu2", dest="mu2",  type=float, default=30.,
                    help="centroid of the second Gaussian, default: %(default)s")

parser.add_argument("-s2", "--sigma2", dest="sigma2",  type=float, default=5.0,
                    help="standard deviation of the second Gaussian, default: %(default)s")

parser.add_argument("-n2", "--norm2", dest="norm2", type=float, default=0.50,
                    help="normalisation of second Gaussian, ]0,1[, , default: %(default)s")

parser.add_argument("-o", "--output", dest="filename", default='py_gauss',
                    help="base name of .coe file, , default: %(default)s")

args = parser.parse_args()
print ("INFO: Generating .coe file with parameters:")
for arg in vars(args):
    print ('  ', arg, getattr(args, arg))  

### CONFIGURATION BLOCK ###
#general parameters
prob_intervals = args.prob_intervals #64
prob_dist_intervals = prob_intervals * 16             
min_size = args.min_frag_size        #min fragment size
max_size = args.max_frag_size        #max fragment size
#parameters for single gaussian
mu1 = args.mu1         #centroid 1
sigma1 = args.sigma1   #
#parameters for double gaussian
norm2 = args.norm2      #normalisation 2
norm1 = 1.0 - norm2     #normalisation 1
mu2 = args.mu2          #centroid 2
sigma2 = args.sigma2    #
#.coe and .png filename
filename = args.filename+'_'+args.dist_choice[0]+"_m"+"{:.0f}".format(mu1)+"_s"+"{:.0f}".format(sigma1)
if args.dist_choice == 'double':
    filename+="_n"+"{:.0f}".format(norm1*100)+"_m"+"{:.0f}".format(mu2)+"_s"+"{:.0f}".format(sigma2)+"_n"+"{:.0f}".format(norm2*100)  

### BI-NORMAL PDF ###
class double_normal(rv_continuous): 
    "double normal"
    def _pdf(self, x, norm1, mu1, sigma1, norm2, mu2, sigma2):
        return norm1*norm.pdf(x,mu1, sigma1)+norm2*norm.pdf(x,mu2,sigma2)       
    def _cdf(self, x, norm1, mu1, sigma1, norm2, mu2, sigma2):
        return norm1*norm.cdf(x,mu1,sigma1)+norm2*norm.cdf(x,mu2,sigma2)
double_norm = double_normal(name = "double_norm", a = 0)

### START OF TABLE GENERATION ###
pdf = 0; cdf = 0
#Evenly spaced numbers over the range. Endpoint included.
x = np.linspace(min_size, max_size, prob_dist_intervals+1)

if args.dist_choice == 'single': 
    pdf = norm.pdf(x, mu1, sigma1)
    cdf = norm.cdf(x, mu1, sigma1)
elif args.dist_choice == 'double': 
    pdf = double_norm.pdf(x, norm1, mu1, sigma1, norm2, mu2, sigma2)
    cdf = double_norm.cdf(x, norm1, mu1, sigma1, norm2, mu2, sigma2)
else: 
    print ('ERROR: pdf not selected')
    exit() 

tab_entry_bounds_est = []
cumul = []

prob_interval_size = np.floor(prob_dist_intervals / prob_intervals)

for i in range(0, prob_dist_intervals+1-int(prob_interval_size), int(prob_interval_size)):
    tab_entry_bounds_est.append(tab_entries*cdf[i])
    cumul.append(cdf[i])

#in view of symmetry, determine actual table entry boundaries 
#left and right of peak with floor and ceil functions resp.
#TODO: what if the distribution is not symmetric w.r.t the median? Shall use the mean?
tab_entry_bounds = [0.0]
for i in range(0,prob_intervals-1):
    if cumul[i]<0.5:
        if (np.floor(tab_entry_bounds_est[i]) < np.floor (tab_entry_bounds_est[i+1])):
            tab_entry_bounds.append(np.floor(tab_entry_bounds_est[i+1]))
    else:
        if(np.ceil(tab_entry_bounds_est[i+1]) > np.ceil(tab_entry_bounds_est[i])):
            tab_entry_bounds.append(np.ceil(tab_entry_bounds_est[i+1]))        

actual_tab_entries = len(tab_entry_bounds)
print ("INFO: Nominal number of address groups", prob_intervals)
print ("INFO: Actual number of address groups", actual_tab_entries)

# Depending on the distribution it can happen that the end boundary will not
# be set equal to tab_entries (in tableEntryBoundaries), therefore
# the latter boundary is added here to tab_entry_bounds_est if needed
# and the count of the boundaries is incremented
if  (tab_entry_bounds[-1] != tab_entries):
    tab_entry_bounds.append(tab_entries)
    actual_tab_entries +=1

#tab_entry_bounds % contents counting from zero!
#transform from table entry boundaries back to size interval boundaries
prob_boundaries = [tb/tab_entries for tb in tab_entry_bounds]
group_boundary_idx_size = [0] #first index needs to be one

for i in range(1, actual_tab_entries-1): 
    #First and last boundary excluded (norminv would output infinity)
    if(args.dist_choice == 'single'):
        k = norm.ppf(prob_boundaries[i],mu1,sigma1)
    elif(args.dist_choice == 'double'):
        k = double_norm.ppf(prob_boundaries[i],norm1, mu1, sigma1, norm2, mu2, sigma2)   
    idx_size = (k - min_size) * prob_dist_intervals / (max_size - min_size)
    group_boundary_idx_size.append(round(idx_size))

group_boundary_idx_size.append(prob_dist_intervals)

# determine the average size for each size interval and for a check also
# the probabilities calculated from the distribution and from the number of
# table entries
av0 = x*pdf
avg_size_prob_group = []
th_prob_group = []
real_prob_group = []

for i in range(0, actual_tab_entries-1):
    idx_dn = int(group_boundary_idx_size[i])
    idx_up = int(group_boundary_idx_size[i+1])
    avg_size_prob_group.append( sum(av0[idx_dn:idx_up])/sum(pdf[idx_dn:idx_up]) )
    th_prob_group.append( sum(pdf[idx_dn:idx_up])/sum(pdf) )
    real_prob_group.append( (tab_entry_bounds[i+1]-tab_entry_bounds[i])/tab_entries )

th_prob = sum(th_prob_group)
real_prob = sum(real_prob_group)

frag_sizes_raw = []
no_prob_intervals = 0

for i in range(0,tab_entries):
    if (i+1 > tab_entry_bounds[no_prob_intervals+1]):
        no_prob_intervals += 1
    frag_sizes_raw.append(avg_size_prob_group[no_prob_intervals])

frag_sizes = [int(round(r)) for r in frag_sizes_raw]

bin_boundaries = [min_size]
for i in range(0,actual_tab_entries-2):
    bin_boundaries.append((avg_size_prob_group[i] + avg_size_prob_group[i+1])/2.0)
bin_boundaries.append(max_size)

### PNG OUTPUT ###
plt.figure(dpi=200)
plt.hist(frag_sizes, bins=bin_boundaries, facecolor='green', alpha=0.75, rwidth=0.4)
plt.xlabel('Fragment size (words)')
plt.ylabel('Address group size')
plt.title('Fragment size probability distribution')
plt.savefig(filename+'.png', dpi=300)
print ("INFO: "+filename+".png printed.")

### COE OUTPUT ####
header = """;*********************************************************************
;********  Gaussian distributrion Block Memory .COE file (JV,CG) *****
;*********************************************************************
;
; This .COE file specifies initialization values for a block 
; memory of depth=1024, and width=16. The values are specified 
; in hexadecimal format.
memory_initialization_radix=16;
memory_initialization_vector=
"""
f = open(filename+'.coe', 'w')
f.write(header)
for i in range(0,len(frag_sizes)-1):
    f.write( str("%04X" % frag_sizes[i])+",\n" )
f.write(str("%04X" % frag_sizes[-1])+";\n" )
f.close()
print ("INFO: "+filename+".coe printed.")
