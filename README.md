```
usage: generate.py [-h] [-a MIN_FRAG_SIZE] [-b MAX_FRAG_SIZE]
                   [-nfs PROB_INTERVALS] [-d DIST_CHOICE] [-m MU1] [-s SIGMA1]
                   [-m2 MU2] [-s2 SIGMA2] [-n2 NORM2] [-o FILENAME]

optional arguments:
  -h, --help            show this help message and exit
  -a MIN_FRAG_SIZE, --min_frag_size MIN_FRAG_SIZE
                        minimum fragment size, default: 0.0
  -b MAX_FRAG_SIZE, --max_frag_size MAX_FRAG_SIZE
                        maximum fragment size, default: 400.0
  -nfs PROB_INTERVALS, --number_fz PROB_INTERVALS
                        nominal number of address groups, default: 64
  -d DIST_CHOICE, --distr DIST_CHOICE
                        single (normal pdf) or double (bi-normal pdf), default: single
  -m MU1, --mu MU1      centroid, default: 200.0
  -s SIGMA1, --sigma SIGMA1
                        standard deviation, default: 50.0
  -m2 MU2, --mu2 MU2    centroid of the second Gaussian, default: 30.0
  -s2 SIGMA2, --sigma2 SIGMA2
                        standard deviation of the second Gaussian, default: 5.0
  -n2 NORM2, --norm2 NORM2
                        normalisation of second Gaussian, ]0,1[, , default: 0.5
  -o FILENAME, --output FILENAME
                        base name of .coe file, , default: py_gauss
```
